package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	// declare router
	router := gin.Default()

	// serve
	router.Run(":8080")
}
