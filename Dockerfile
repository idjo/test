FROM gcr.io/distroless/base-debian12

WORKDIR /app

ENV GIN_MODE=release

ENV READINESS_CHECK_PORT=8080

ENV PORT=8080

COPY ./bin/test .

COPY . .

CMD ["/app/test"]
